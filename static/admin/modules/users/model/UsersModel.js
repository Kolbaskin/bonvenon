/**
 * @author Max Tushev
 * @scope Server, Client
 * The model for Users module 
 * @private
 */
Ext.define('Desktop.modules.users.model.UsersModel', {    
    extend: "Core.data.DataModel"

    ,collection: 'admin_users'

    ,fields: [
    {
        name: '_id',
        type: 'ObjectID',
        visable: true
    },
    {
        name: 'login',
        type: 'string',
        filterable: true,
        unique: true,
        editable: true,
        visable: true
    },
    {
        name: 'name',
        type: 'string',
        filterable: true,
        editable: true,
        visable: true
    },
    {
        name: 'email',
        type: 'string',
        filterable: true,
        vtype: 'email',
        editable: true,
        visable: true
    },
    {
        name: 'dblauth',
        type: 'boolean',
        editable: true,
        visable: true
    },
    {
        name: 'pass',
        type: 'password',
        editable: true,
        visable: false
    },
    {
        name: 'groupid',
        type: 'ObjectID',
        editable: true,
        visable: true
    },{
        name: 'xgroups',
        type: 'arraystring',
        editable: true,
        visable: true,
        itemsType: 'ObjectID'
    },{
        name: 'tel',
        type: 'string',
        filterable: true,
        editable: true,
        visable: true
    },{
        name: 'act1',
        type: 'boolean',
        filterable: true,
        editable: true,
        visable: true
    },{
        name: 'state',
        type: 'int',
        filterable: false,
        editable: true,
        visable: true
    }]
    
    ,changeUserState: function(_id, state, cb) {
        var me = this;
        [
            function(next) {
                if(Ext.isString(_id)) {
                    me.src.db.fieldTypes.ObjectID.getValueToSave(me, _id, null, null, null, function(id) {
                        _id = id;
                        next()
                    })
                } else 
                    next()
            }
            ,function(next) {
                me.src.db.collection(me.collection).findOne({_id: _id}, {_id: 1, login: 1}, function(e, u) {
                    if(u) {
                        next(u)    
                    }
                })
            }
            ,function(user) {
                me.src.db.collection(me.collection).update({_id: user._id}, {$set: {state: state}}, function(e, u) {
                    user.state = state;
                    me.changeModelData(Object.getPrototypeOf(me).$className, 'changeState', user)
                    cb()
                })
            }
        ].runEach()
    }
})