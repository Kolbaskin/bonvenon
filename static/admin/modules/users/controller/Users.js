/*!
 * Wpier
 * Copyright(c) 2006-2011 Sencha Inc.
 * 
 * 
 */


Ext.define('Desktop.modules.users.controller.Users', {
    extend: 'Core.controller.Controller',
    id:'users-win',

    launcher: {
        text: D.t('Users'),
        iconCls:'user-admin'        
    }
    
});

