/*!
 * Wpier
 * Copyright(c) 2006-2011 Sencha Inc.
 * 
 * 
 */

Ext.define('Desktop.modules.users.controller.Main', {
    extend: 'Core.controller.MenuController',
    id:'users-menu-win',    
    launcher: {
        text: D.t('Users'),
        iconCls:'users',        
        menu: {
            items: [{
                text: D.t('Admins'),
                iconCls:'user-admin',
                controller: 'Desktop.modules.users.controller.Users'
            },{
                text: D.t('Admins groups'),
                iconCls:'user-group',
                controller: 'Desktop.modules.users.controller.Groups'
            },{
                text: D.t('User logs'),
                iconCls:'logs',
                controller: 'Desktop.modules.logs.controller.Logs'
            },'-',{
                text: D.t('Users on site'),
                iconCls:'user-publ',
                controller: 'Desktop.modules.users.controller.PublUsers'
            }]
        }
    }        
});

