

Ext.define('Desktop.modules.templates.view.TemplatesList', {
    extend: 'Core.grid.GridWindow'
    
    ,filtrable: true
    ,sortManually: true
    
    ,buildColumns: function() {
        return [
            new Ext.grid.RowNumberer(),
            {
                text: D.t("Image"),
                flex: 1,
                sortable: true,
                dataIndex: 'img',
                
                renderer: function(value) {
                    return "<img src='" + value + "?_dc=" + (new Date()).getTime() + "' />";    
                }
            },{
                text: D.t("Template name"),
                flex: 1,
                sortable: true,
                filter: true,
                dataIndex: 'name'
            },{
                text: D.t("Count of blocks"),
                flex: 1,
                sortable: true,
                dataIndex: 'blocks'
            }               
            
            ,{
                text: D.t("Template"),
                flex: 1,
                sortable: false,
                filter: true,
                dataIndex: 'tpl'
            }
            ,{
                text: D.t("Controller"),
                flex: 1,
                sortable: false,
                filter: true,
                dataIndex: 'controller'
            }
        ]        
    }
})