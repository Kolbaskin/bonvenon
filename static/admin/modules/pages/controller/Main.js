/*!
 * Wpier
 * Copyright(c) 2006-2011 Sencha Inc.
 * 
 * 
 */

Ext.define('Desktop.modules.pages.controller.Main', {
    extend: 'Core.controller.MenuController',
    
    launcher: {
        text: D.t('Site tools'),
        iconCls:'pages',
        menu: {
            items: [                    
            {
                text: D.t('Pages'),
                iconCls:'pages',
                controller: 'Desktop.modules.pages.controller.Pages'
            },{
                text: D.t('Main menu'),
                iconCls:'mainmenu',
                controller: 'Desktop.modules.mainmenu.controller.Menu'
            },{
                text: D.t('Templates'),
                iconCls:'templates',
                controller: 'Desktop.modules.templates.controller.Templates'
            },{
                text: D.t('File manager'),
                iconCls:'filemanager',
                controller: 'Desktop.modules.filemanager.controller.fm'
            },'-',{
                text: D.t('Rebuilding of search index'),
                iconCls:'refresh',   
                model: 'pages-SearchReindex',
                handler: function() {
                    D.c('Rebuilding of search index', 'Old indexes will be removed. Are You sure?', [], function() {
                        Core.Ajax.request({
                            url:'/search.engine:rebuildAll',
                            callback: function(a1, a2, a3) {
                                D.a('Search index was rebuilt.')
                            }
                        })
                    })
                }
            },{
                text: D.t('Debugger'),
                iconCls:'debugger',
                controller: 'Desktop.modules.debugger.controller.Debugger'
            },{
                text: D.t('Search'),
                iconCls:'search',
                controller: 'Desktop.modules.search.controller.Search'
            }
            ]
        }
    }
    


});

