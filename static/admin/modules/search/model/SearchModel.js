Ext.define('Desktop.modules.search.model.SearchModel', {    
    extend: "Core.data.DataModel"
    
    ,collection: 'admin_templates'

    ,fields: [{
        name: '_id',
        type: 'ObjectID',
        visable: true
    },{
        name: 'name',
        type: 'string',
        filterable: true,
        unique: true,
        editable: true,
        visable: true
    },{
        name: 'blocks',
        type: 'number',
        editable: true,
        filterable: true,
        visable: true
    },{
        name: 'tpl',
        type: 'string',
        editable: true,
        filterable: true,
        visable: true
    },{
        name: 'controller',
        type: 'string',
        editable: true,
        filterable: true,
        visable: true
    },{
        name: 'img',
        type: 'image',
        editable: true,
        visable: true
    },{
        name: 'indx',
        type: 'sortfield',
        sort: 1,
        filterable: true,
        editable: true,
        visable: true
    }]
    
    ,getData: function(params, cb) {
        var me = this
            ,search = me.src.elasticsearch;
            
        console.log(params);
        
        
        cb({total:0, list:[]})
        
    }
    
})