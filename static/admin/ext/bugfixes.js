isLoading = false;
isLoadingTout = null;
mainViewportPanel = null;

Ext.onReady(function () {
    
    
    Ext.override(Ext, {
        create: function() {
            
            if(!Ext.isLoading) {
                Ext.isLoading = true;
                GlobalLoadingMask.style.display = "block"
            }
            
            var res = (Ext.ClassManager? Ext.ClassManager.instantiate.apply(Ext.ClassManager, arguments): null)
            
            if(Ext.isLoading) {
                if(Ext.isLoadingTout) clearTimeout(Ext.isLoadingTout) 
                Ext.isLoadingTout = setTimeout(function() {
                    GlobalLoadingMask.style.display = "none"
                    Ext.isLoading = false
                }, 300)
            }

            return res
        }
    })
    
    Ext.override(Ext.selection.Model, {
        storeHasSelected: function (record) {
            var store = this.store,
                records,
                len, id, i;
            if (record.hasId() && store.getById(record)) {
                return true;
            }

            if (store.data instanceof Ext.util.LruCache) {
                var result = false;
                store.data.forEach(function (rec) {
                    return !(result = (record.internalId === rec.internalId));
                });
                return result;
            }

            records = store.data.items;
            len = records.length;
            id = record.internalId;
            for (i = 0; i < len; ++i) {
                if (id === records[i].internalId) {
                    return true;
                }
            }
            return false;
        }
    });
    
    Ext.define('Ext.override.grid.ViewDropZone', {
        override: 'Ext.grid.ViewDropZone',
    	handleNodeDrop: function()
    	{
    		var sm = this.view.getSelectionModel(),
    			onLastFocusChanged = sm.onLastFocusChanged;
    
    		sm.onLastFocusChanged = Ext.emptyFn;
    		this.callParent(arguments);
    		sm.onLastFocusChanged = onLastFocusChanged;
    	}
    });
    
    Ext.override(Ext.window.Window, {
        border: false,
        bodyBorder: false,
        bodyStyle: 'background: #ffffff',
        fitContainer: function(animate) {
            var me = this,
                parent = me.floatParent,
                container = parent ? parent.getTargetEl() : me.container,
                newBox = container.getViewSize(false),
                newPosition = container.getXY();            
            newBox.x = newPosition[0];
            newBox.y = newPosition[1];
            me.setBox(newBox, animate);
        }
    });
    
    Ext.define('Ext.form.field.Date', {
        extend: 'Ext.form.field.Date',
        initComponent: function(cfg) {
            if(!this.format || this.format == 'm/d/Y' || this.format == 'm/d/y') {
                this.format = D.t('m/d/Y')
            }
            this.submitFormat = 'm/d/Y'
            this.altFormats = 'c|m/d/Y|m/d/y'
            this.callParent(arguments)
        }
    })
    
    Ext.define('Ext.grid.column.Date', {
        extend: 'Ext.grid.column.Date',
        initComponent: function() {
            if(!this.format) this.format = D.t('m/d/Y')
            this.callParent(arguments)
        }
    })
    
    Ext.define('Ext.form.field.Checkbox', {
        extend: 'Ext.form.field.Checkbox',
        initComponent: function() {
            this.uncheckedValue = false
        }
    })
    
    Ext.override(Ext.util.Renderable, {
        afterRender : function() {
            var me = this,
                data = {},
                protoEl = me.protoEl,
                target = me.el,
                item, pre, hide, contentEl;
    
            me.finishRenderChildren();
            
            
            
            if (me.contentEl) {
                pre = Ext.baseCSSPrefix;
                hide = pre + 'hide-';
                contentEl = Ext.get(me.contentEl);
                contentEl.removeCls([pre+'hidden', hide+'display', hide+'offsets', hide+'nosize']);
                me.getContentTarget().appendChild(contentEl.dom);
            }
    
            if(protoEl) protoEl.writeTo(data);
            
            
            
            
            item = data.removed;
            if (item) {
                target.removeCls(item);
            }
            
            item = data.cls;
            if (item.length) {
                target.addCls(item);
            }
            
            item = data.style;
            if (data.style) {
                target.setStyle(item);
            }
            
            me.protoEl = null;
    
            
            if (!me.ownerCt) {
                me.updateLayout();
            }
        }
    })
    
    Ext.override(Ext.menu.Menu, {
        onMouseLeave: function(e) {
        var me = this;
    
    
        // BEGIN FIX
        var visibleSubmenu = false;
        me.items.each(function(item) { 
            if(item.menu && item.menu.isVisible()) { 
                visibleSubmenu = true;
            }
        })
        if(visibleSubmenu) {
            //console.log('apply fix hide submenu');
            return;
        }
        // END FIX
    
    
        me.deactivateActiveItem();
    
    
        if (me.disabled) {
            return;
        }
    
    
        me.fireEvent('mouseleave', me, e);
        }
    });
    
})
