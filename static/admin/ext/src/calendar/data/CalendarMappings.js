//@define Ext.calendar.data.CalendarMappings
/**
 * @class Ext.calendar.data.CalendarMappings
 * @extends Object
 * A simple object that provides the field definitions for Calendar records so that they can be easily overridden.
 */
Ext.ns('Ext.calendar.data');

Ext.calendar.data.CalendarMappings = {
    CalendarId: {
        name:    '_id',
        mapping: '_id',
        type:    'string'
    },
    Title: {
        name:    'title',
        mapping: 'title',
        type:    'string'
    },
    Description: {
        name:    'desc', 
        mapping: 'desc',   
        type:    'string' 
    },
    ColorId: {
        name:    'color',
        mapping: 'color',
        type:    'int'
    },
    IsHidden: {
        name:    'hidden',
        mapping: 'hidden',
        type:    'boolean'
    }
};