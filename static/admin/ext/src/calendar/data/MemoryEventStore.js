Ext.define('Ext.calendar.data.MemoryEventStore', {
    extend: 'Core.data.Store',

    prepData: function(res) {
        
        for(var i=0;i<res.count;i++) {
            res.records[i].data.start = new Date(res.records[i].data.start)
            res.records[i].data.end = new Date(res.records[i].data.end)
        }
        
        return res;
        
    }

});