//@define Ext.calendar.data.EventMappings
/**
 * @class Ext.calendar.data.EventMappings
 * @extends Object
 * A simple object that provides the field definitions for Event records so that they can be easily overridden.
 */
Ext.ns('Ext.calendar.data');

Ext.calendar.data.EventMappings = {
    EventId: {
        name: '_id',
        mapping: '_id',
        type: 'string'
    },
    CalendarId: {
        name: 'cid',
        mapping: 'cid',
        type: 'int'
    },
    Title: {
        name: 'title',
        mapping: 'title',
        type: 'string'
    },
    StartDate: {
        name: 'start',
        mapping: 'start',
        type: 'date',
        dateFormat: 'c'
    },
    EndDate: {
        name: 'end',
        mapping: 'end',
        type: 'date',
        dateFormat: 'c'
    },
    Location: {
        name: 'Location',
        mapping: 'loc',
        type: 'string'
    },
    Notes: {
        name: 'notes',
        mapping: 'notes',
        type: 'string'
    },
    Url: {
        name: 'url',
        mapping: 'url',
        type: 'string'
    },
    IsAllDay: {
        name: 'ad',
        mapping: 'ad',
        type: 'boolean'
    },
    Reminder: {
        name: 'ewm',
        mapping: 'rem',
        type: 'string'
    },
    IsNew: {
        name: 'n',
        mapping: 'n',
        type: 'boolean'
    }
};
