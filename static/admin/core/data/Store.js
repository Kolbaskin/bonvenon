/**
 * @class Core.data.Store
 * @extend Ext.data.Store
 * @private
 * @author Max Tushev <maximtushev@gmail.ru>
 * 
 * Yode data store base class.
 * 
 *     @example *     
 *     var store = Ext.create('Core.data.Store',{
 *         dataModel: 'My.Model.Class.Name',
 *         scope: window, // Specify a link to window there this store is used
 *         fieldSet: ['_id', 'name'] // List the fields that you need
 *     })
 */
Ext.define('Core.data.Store', {
    extend: 'Ext.data.Store',
    
    requires: [
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.ux.data.proxy.WebSocket'
    ],
                             
    constructor: function(options) {        
        Ext.apply(this, options)
       
        if(this.autoLoad === undefined) this.autoLoad = true
        
        if(!options) options = {}
        
        this.initModel({
            dataModel: options.dataModel || this.dataModel,
            fieldSet: options.fieldSet || this.fieldSet
        })
        this.initDataGetter({
            dataModel: options.dataModel || this.dataModel,
            fieldSet: options.fieldSet || this.fieldSet,
            scope: options.scope || this.scope
        }) 

        this.callParent(arguments);

    }
    
    /**
     * @method
     * @private
     * Setting up proxy and reader
     * @param {Object} options
     */ 
    ,initDataGetter: function(options) {
        var me = this;
        
        me.remoteSort = true
        me.remoteGroup = true
        me.remoteFilter = true
        me.data = []
        me.pageSize = 50
             
        me.proxy = me.createProxy(options)
        
        setTimeout(function() {
            me.createReader()
            if(me.autoLoad) {me.load();}
        }, 100)
        
        if(options.scope) this.scope = options.scope        
        me.dataActionsSubscribe()
    }
    
    /**
     * @method
     * @private
     * Model initialisation
     * @param {Object} options
     */
    ,initModel: function(options) {
        var me = this
            ,modelPath
        
        me.id = 'store-' + (new Date()).getTime()+Math.random();
       
        if(options.dataModel) {
            if(Ext.isString(options.dataModel)) {
                me.dataModel = Ext.create(options.dataModel)
                me.modelPath = options.dataModel
            } else {    
                me.dataModel = options.dataModel
                me.modelPath = Object.getPrototypeOf(options.dataModel).$className
                
            }
        }
        
        if(options.fieldSet) {
            if(Ext.isString(options.fieldSet))
                options.fieldSet = options.fieldSet.split(',')
            me.fields = [{name: '_id'}] 
            
            for(var i=0;i<options.fieldSet.length;i++) 
                me.fields.push({name: options.fieldSet[i]})
        } else {
            me.fields = options.dataModel.fields.items
        }
    }
    
    /**
     * @method
     * @private
     * @param {Object} options
     */
    ,createProxy: function(options) {
        var me = this;
        
        /*
        return {
            type: 'ajax',
            url: Sess.url('/'+me.modelPath+'.read/'),
            simpleSortMode: true,
            filterParam: 'query',
            remoteFilter: true
        }
        */
        
        return {
            type: 'websocket',
            storeId: me.id,
            websocket: Core.ws,
            params: {
                model: me.modelPath,
                scope: me.id,
                fieldSet: options.fieldSet   
            },
            simpleSortMode: true,
            filterParam: 'query',
            remoteFilter: true
        }
    }
    
    /**
     * @method
     * @private
     */
    ,createReader: function() {
        this.getProxy().setReader(Ext.create('Ext.data.reader.Json', {
             root: 'list'
             ,totalProperty: 'total'
             ,successProperty: 'success'
        }))
    }
    
    /**
     * @method
     * Subscribing this store to the server model events
     */
    ,dataActionsSubscribe: function() {
        var me = this
            ,wid = (me.scope? me.scope.id: me.modelPath);
        
        if(me.scope) {
            // remove subscription when window is closed
            me.scope.on('destroy', function() {
                Core.ws.unsubscribe(wid)    
            })        
        }
        Core.ws.subscribe(wid, me.modelPath, function(action, data) {
            me.onDataChange(action, data)
        })
    }
    
    /**
     * @method
     * This method fires when the model data has been changed on the server
     * @param {String} action one of ins, upd or remove
     * @param {Object} data
     */
    ,onDataChange: function(action, data) {
        var me = this
        if(!Ext.isArray(data)) data = [data]
        switch(action) {
            case 'ins'   : me.insertData(data);break;
            case 'upd'   : me.updateData(data);break;
            case 'remove': me.removeData(data);break;
        }   
    }
    
    /**
     * @method
     * The action for inserting
     * @paramn {Object} data
     */
    ,insertData: function(data) {
        this.reload()
        //this.insert(0, data)
    }
    
    /**
     * @method
     * The action for updating
     * @paramn {Object} data
     */
    ,updateData: function(data) {        
        var rows = this.data.items;
        for(var i=0;i<rows.length;i++) {
            for(var j=0;j<data.length;j++) {
                if(rows[i].data._id == data[j]._id) {
                    for(var k in rows[i].data) {
                        if(data[j][k] && data[j][k] != rows[i].data[k]) rows[i].data[k] = data[j][k]
                    }
                    rows[i].commit()
                    break;
                }
            }
        }
    }
    
    /**
     * @method
     * The action for removing
     * @paramn {Object} data
     */
    ,removeData: function(data) { 
        
        this.reload()
    }
});