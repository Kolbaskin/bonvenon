/**
 * @class Core.data.StoreTree
 * @extend Ext.data.TreeStore
 * @private
 * @author Max Tushev <maximtushev@gmail.ru>
 * This is a class for store for tree panel. Use Core.data.StoreTree like as {@link Core.data.Store}  
 */
Ext.define('Core.data.StoreTree', {
    extend: 'Ext.data.TreeStore'
    
    ,constructor: function(cnf) {
        Ext.apply(this, cnf)
        this.dataActionsSubscribe()        
        this.callParent(arguments);
    }
    
    /**
     * @method
     * Subscribing this store to the server model events
     */
    ,dataActionsSubscribe: function() {
        var me = this
            ,wid = (me.scope? me.scope.id: me.modelPath);
        
        if(me.scope) {
            // remove subscription when container is closed
            me.scope.on('destroy', function() {
                Core.ws.unsubscribe(wid)    
            })        
        }
        Core.ws.subscribe(wid, me.modelPath, function(action, data) {
            me.onDataChange(action, data)
        })
    }
    
    /**
     * @method
     * This method fires when the model data has been changed on the server
     * @param {String} action one of ins, upd or remove
     * @param {Object} data
     */
    ,onDataChange: function(action, data) {
        var me = this
        if(!Ext.isArray(data)) data = [data]
       
        switch(action) {
            case 'ins'   : me.insertData(data);break;
            case 'upd'   : me.updateData(data);break;
            case 'remove': me.removeData(data);break;
        }   
    }
    
    /**
     * @method
     * The action for inserting
     * @paramn {Object} data
     */
    ,insertData: function(data) {
        //this.insert(0, data)
    }
    
    /**
     * @method
     * The action for updating
     * @paramn {Object} data
     */
    ,updateData: function(data) {        
        var me = this       
        var func = function(nodes) {
            for(var i=0;i<nodes.length;i++) {
                if(nodes[i].childNodes && nodes[i].childNodes.length)  func(nodes[i].childNodes)
                for(var j=0;j<data.length;j++) {
                    if(data[j]._id == nodes[i].data._id) {
                        Ext.apply(nodes[i].data, data[j])
                        nodes[i].commit();
                        break;
                    }
                }
            }
        }
        
        func(me.getRootNode().childNodes)
    }
    
    /**
     * @method
     * The action for removing
     * @paramn {Object} data
     */
    ,removeData: function(data) { 
        // action to removing data
    }

});