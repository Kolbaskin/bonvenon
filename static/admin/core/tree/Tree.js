Ext.define('Core.tree.Tree', {
    extend: 'Ext.tree.Panel',
    
    requires: [
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.tree.*',
        'Ext.ux.data.proxy.WebSocket'
    ],    
    
    viewConfig: {
            plugins: {
                ptype: 'treeviewdragdrop',
                containerScroll: true,
                enableDrag: true,
                enableDrop: true
                ,appendOnly: false
                ,displayField: 'name'
                
            }
    },
    
    //useArrows: true,
    multiSelect: false,
    //singleExpand: true,
    rootVisible: false,
            
    initComponent: function() {    
        var me = this
            ,modelPath = Object.getPrototypeOf(me.model).$className
            ,id = 'store-' + (new Date()).getTime()+Math.random()
            ,proxyParams = {
                    model: modelPath,
                    scope: id
                    //fieldSet: options.fieldSet.join(',')   
                }
        
        if(me.fieldSet)
            proxyParams.fieldSet = me.fieldSet
        
        Ext.define (modelPath + '_inn', {
            extend: modelPath ,
        	//fields: me.model.fields.items,
    		proxy: {
                type: 'websocket',
                storeId: id,
                websocket: Core.ws,
                params: proxyParams,
                reader: {
                     type: 'json',
                     root: 'list'
                }
            }
    	});
      
        this.store = Ext.create('Core.data.StoreTree',{
            model: modelPath + '_inn',
            modelPath: modelPath,
            storeId: id,
            scope: me.scope,
            sorters: [{
                property: 'indx',
                direction: 'ASC'
            }],
    		root: me.rootNode()
        }) 
        this.tbar = this.buildButtons()
        this.columns = this.buildColumns()
        this.callParent();
    }
    
    ,rootNode: function() {
        return {
        	text: 'Root' ,
    		expanded: true
    	}
    }
    
    ,buildColumns: function() {
        return [{
                xtype: 'treecolumn',
                text: D.t('Name'),
                flex: 1,
                //sortable: true,
                dataIndex: 'name'
            }]
    }
    
    ,buildButtons: function() {
        return []
    }
});