/**
 * @class Core.form.DetailForm
 * @extend Ext.form.Panel
 * @author Max Tushev
 * This is a main class for a detail forms.
 * 
 *     @example
 *     Ext.define('myNamespace.module.myModule.view.myModuleForm', {
 *         extend: 'Core.form.DetailForm',
 *         buildItems: function() {
 *             return [
 *                 {
 *                     name: 'fieldName',
 *                     fieldLabel: 'Field label'
 *                 },
 *                 {
 *                     name: 'fieldName1',
 *                     fieldLabel: 'Field label 1',
 *                     xtype: 'textarea'
 *                 }
 *             ]
 *         }
 *     });
 * 
 */
Ext.define('Core.form.DetailForm', {
    extend: 'Ext.form.Panel',
    
    defaults: {
        labelWidth: 90,
        xtype: 'textfield',
        margin: '5'
    },
    
    border: false,
    bodyBorder: false,
        
    initComponent: function() {        
        this.items = this.buildItems()         
        this.items.push({xtype: 'textfield', name: '_id', region: 'west', inputType:'hidden'})        
        this.tbar = this.buildTbar()        
        this.buttons = this.buildButtons()   

        this.callParent();
    }
    
    /**
     * @method
     * Returns form items
     */
    ,buildItems: function() {
        return []
    }
    
    /**
     * @method
     */
    ,buildTbar: function() {
        return null
    }
    
    /**
     * @method
     */
    ,buildButtons: function() {
        return [
            {tooltip: D.t('Remove this record'), ui:'remove', action: 'remove'},
            '->',
            {
                text: D.t('Save and close'), 
                ui: 'primary',
                scale: 'medium',
                action: 'formsave'},
            {text: D.t('Save'), scale: 'medium',action: 'formapply'},
            '-',
            {text: D.t('Close'), scale: 'medium',action: 'formclose'}
            
            
        ]    
    }
    
    /**
     * @method
     */
    ,buildButtonsPined: function() {
        return [
            {
                text: D.t('Save'),
                action: 'formapply',
                //iconCls: 'save'
                ui: 'primary'
            }
        ]
    }

    ,setValues: function(row) {
        this.getForm().setValues(row)    
    }
    

})